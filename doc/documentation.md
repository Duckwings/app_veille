#Bienvenue sur le guide de l'application Veille de Popschool

Vous trouverez au travers de ce guide toutes les informations pour bien commencer
avec l'application Veille de Popschool. N'hésitez pas à contacter l'administrateur
si vous ne trouvez pas réponse à vos questions.

##Sommaire :

1. Création de votre compte utilisateur.
2. Connexion à votre compte utilisateur.
3. Déconnexion de votre compte utilisateur.
4. Modification d'une veille.
5. Suppression d'une veille.

  ###Création de votre compte utilisateur.

Si vous n'avez pas encore de compte utilisateur pour poster votre contenu, vous pouvez en créer un gratuitement en cliquant sur le lien "Créer mon compte utilisateur" dans la barre de navigation. Une fois sur la page contenant le formulaire d'inscription, renseignez les champ comme suit :

* Votre identifiant de connexion : choisissez un pseudonyme unique pour vous connecter à votre session.
* Votre nom : veuillez remplir ce champ avec votre véritable nom de famille afin de pouvoir facilement vous reconnaitre.
* Votre prénom : veuillez remplir ce champ avec votre véritable prénom afin de pouvoir facilement vous reconnaitre.
* Promotion : choisissez dans la liste déroulante la promotion dont vous faites partie. Si cette dernière n'apparait pas dans la liste, demandez à votre administrateur de l'ajouter à la liste.
* Mot de passe : choisissez un mot de passe pour la connexion à votre session utilisateur.
* Confirmer le mot de passe : veuillez retaper à l'identique le mot de passe que vous avez choisi précédemment.
* Photo : envoyez une photo de profil. Cette dernière ne doit pas excéder la limite de 5Mb.

Une fois tous les champs remplis, vous pouvez valider votre incription en cliquant sur le bouton "s'inscrire". En cas d'erreurs, vérifiez si tous les champs sont convenablement remplis.

  ###Connexion à votre compte utilisateur.

Si vous possédez déjà un compte utilisateur, cliquez sur "Connexion" dans la barre de navigation. Remplissez les champs de la manière suivante :
* Pseudo : écrivez ici l'identifiant de connexion que vous avez choisis lors de votre inscription.
* Mot de passe : écrivez ici le mot de passe que vous avez choisi lors de votre inscription.

Une fois les champs précédents convenablement remplis, vous pouvez cliquer sur "Se connecter". En cas d'erreurs, vérifiez que vous n'avez pas de faute lors de votre saisie.

  ###Présentation de votre espace personnel.

Votre espace personnel présente l'ensemble des informations relatives à votre compte. Vous pouvez également modifier certaines de vos informations à partir de là.

  ###Déconnexion de votre compte utilisateur.

Pour vous déconnecter de votre compte, cliquez sur le bouton "Déconnexion" dans la barre de navigation. Vous retournez automatiquement à la page de connexion.

  ###Ajour d'une veille

Pour publier la veille que vous avez selectionnée, cliquez sur le bouton ajouter une veille. Vous arrivez alors sur la page d'ajour de veille. Remplissez alors les champs suivants (ils doivent obligatoirement être remplis) :
* Sujet : identifiez le sujet abordé par votre article.
* Titre : donnez un titre permettant d'identifier l'article que vous avez choisi.
* Catégorie : entrez dans ce champs un mot clé caractérisant votre veille.
* Contenu : cliquez sur le bouton "parcourir..." puis choisissez un document illustrant votre veille. Vous pouvez envoyer des fichiers image comportant les extensions .jpg, .jpeg, .png.

  ###Modification d'une veille.

Pour modifier une veille, rendez-vous sur la page de la veille puis cliquez sur le bouton "Modifier". Complétez les champs suivants :
* Modifier le sujet : choisissez une courte description adapté à l'article partagé.
* Modifier le titre : choisissez un titre adapté à l'article.
* Modifier la catégorie : choisissez une catégorie convenant au thème de l'article.
* Modifier le contenu : remplissez ici le nouveau contenu que vous souhaitez partager.

Une fois tous les champs remplis, vous pouvez cliquer sur "Enregistrer les modifications" pour sauvegarder votre nouvel article.

  ###Suppression d'une veille.

Pour supprimer une veille, rendez-vous sur la page de la veille puis clique sur le bouton "Supprimer".
